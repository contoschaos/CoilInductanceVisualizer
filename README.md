# Coil inductance visualizer

This project is made to better visualize coil properties change based on changing its dimensions and type of wire used.

![Dimensions name of coil](dimensions.png "Dimensions")

## Example of output

Colour is custom fitness visualization.
![3D value graph](fig.png "3D value graph")
