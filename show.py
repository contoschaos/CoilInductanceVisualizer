import time
import datetime
import plotly.graph_objects as go
import numpy as np
import multyloop
import util
import multiprocessing as mp
from enum import Enum

quantityToShow = 0


class QuantityName(Enum):
    Q_FACTOR = "Quality factor for %i hz"
    RESISTIVITY = "Resistivity in Ω"
    MASS = "Mass in gram"
    N_TURN = "Number of turns"


quantityList = list(QuantityName)

results = []


def collect_result(result):
    global results
    index_x, index_y, value = result
    results[index_x][index_y] = value


def worker(index_x, index_y, x_val, y_val):
    result = multyloop.calculate(x_val, y_val)
    return (index_x, index_y, result)


# System specific
now = datetime.datetime.now()
print("Started:\t", '{:%Y-%m-%d %H:%M:%S}'.format(now))
start_time = time.time()

# d - wire diameter
# SWG 48 - 20
x = np.array([0.0406, 0.0610, 0.0813, 0.1016, 0.1219,
              0.1524, 0.1930, 0.2337, 0.2743, 0.3150,
              0.3759, 0.4572, 0.559,  0.711,  0.914])
# 1mH - 150mH - desired inductance
y = np.linspace(1000, 150000, 15)

x_len = len(x)
y_len = len(y)


# Initialize an empty object array to store tuples
results = np.empty((x_len, y_len), dtype=object)

# Populate the array with initial tuples, e.g., (0, 0, 0, 0)
for i in range(x_len):
    for j in range(y_len):
        results[i, j] = (0, 0, 0, 0)

# Flatten the arrays to create a list of pairs with indices
tasks = [(j, i, xi, yi) for i, xi in enumerate(x) for j, yi in enumerate(y)]

# Parallel processing
# protect the entry point
if __name__ == '__main__':
    pool = mp.Pool()
    for index_x, index_y, xi, yi in tasks:
        pool.apply_async(worker, args=(index_x, index_y,
                         xi, yi), callback=collect_result)
    pool.close()
    pool.join()

    z1, z2, z3, z4 = np.zeros((len(x), len(y))), np.zeros(
        (len(x), len(y))), np.zeros((len(x), len(y))), np.zeros((len(x), len(y)))
    for i, row in enumerate(results):
        for j, value in enumerate(row):
            z1[i][j] = value[0]
            z2[i][j] = value[1]
            z3[i][j] = value[2]
            z4[i][j] = value[3]

norm_z1 = util.normalize(z1, threshold=5)
norm_z2 = util.normalize(z2, threshold=np.average(z2))*-1
norm_z3 = util.normalize(z3, threshold=np.average(z3))*-1

weight_q = 5
intensity = (weight_q * norm_z1 + norm_z2 + norm_z3) / (1 + weight_q)

end_time = time.time()
print('Done in:', end_time - start_time, 'seconds')

surface_n = go.Surface(
    x=x, y=y, z=z4, surfacecolor=intensity, text=np.round(z1, 2), hoverinfo='x+y+z+text')

fig = go.Figure(data=[surface_n])

z_axis_title = quantityList[quantityToShow].value
try:
    z_axis_title = z_axis_title % multyloop.FREQ
except TypeError as e:
    pass


fig.update_layout(
    scene=dict(
        xaxis_title='Wire diameter',
        yaxis_title='Inductance in µH',
        zaxis=dict(
            title='Number of turns',
            autorange=True
        )
    )
)

fig.show()
fig.write_html('./StoredFigures/storedImage', auto_open=False)
