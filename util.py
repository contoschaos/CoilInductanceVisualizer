import numpy as np

# Function to normalize using logistic transformation


def normalize(array, threshold=1.0, steepness=10):
    """
    Normalize array using a logistic transformation with specified parameters.
    Args:
        array (np.array): The array of values.
        threshold (float): The value below which the normalization approaches -1.
        steepness (float): Controls the steepness of the logistic function.

    Returns:
        np.array: Weighted and normalized array.
    """
    normalized_array = np.full_like(array, -1, dtype=np.float64)

    # Apply the logistic transformation only to non-negative values
    non_negative_mask = array >= 0
    normalized_array[non_negative_mask] = 1 / \
        (1 + np.exp(-steepness * (array[non_negative_mask] - threshold)))

    return normalized_array
