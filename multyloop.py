from functools import lru_cache
from scipy.special import ellipk, ellipe
import math
import numpy as np

DIAMETER_WIRE_MAX = 2  # Used to limit calculation based on wire diameter, mm
MASS_MAX = 100000  # Used to limit calculation based on total weight, gram
TURNS_MAX = 9999  # Used to limit calculation to max turns

FREQ = 40000  # hz
# D - inner diameter
INNER_DIAMETER = 3   # == 10*mm  # D example 1 == 10mm, 2 == 20mm (cm)
# l - coil length
COIL_LENGTH = 1   # == 1*mm  # lk (cm)

# should be representation of copper material resistivity
MATERIAL_RESISTIVITY = 1.7241e-8 * 1e2  # ohm / m


def calculate(wireDiameter, targetInductance):
    if wireDiameter > DIAMETER_WIRE_MAX:
        return (-1, -1, -1, -1)

    resist, mass, N = getAny(targetInductance, (wireDiameter)/10)

    if mass == -1:
        return (-1, -1, -1, -1)

    qFactor = getQfactor(targetInductance, resist)

    return (qFactor, resist, mass, N)


def getMutalInductance(r1, r2, x, g):
    '''
    For circular \n
    r1,r2 - radii for two filaments \n
    x - distance between centers \n
    g - geometric mean distance 
    '''
    l = math.sqrt(math.pow(r2-r1, 2)+math.pow(x, 2))
    c = 2 * math.sqrt(r1*r2)/math.sqrt(math.pow(r1+r2, 2) + math.pow(l-g, 2))
    Fk, Ek = getElipticIntegralFirstAndSecond(c)
    return -0.004 * math.pi * math.sqrt(r1*r2) * ((c-2/c)*Fk + (2/c)*Ek)


@lru_cache(maxsize=None)
def getMutalInductanceV2(r1, r2, x, g):
    l = math.sqrt(math.pow(r2-r1, 2)+math.pow(x, 2))
    c = 2 * math.sqrt(r1*r2)/math.sqrt(math.pow(r1+r2, 2) + math.pow(l-g, 2))
    Fk, Ek = getElipticIntegralFirstAndSecondV2(c)
    return -0.004 * math.pi * math.sqrt(r1*r2) * ((c-2/c)*Fk + (2/c)*Ek)


def getElipticIntegralFirstAndSecond(c):
    a = 1
    b = math.sqrt(1-math.pow(c, 2))
    E = 1 - math.pow(c, 2)/2
    i = 1

    while (math.fabs(a-b) > 1e-15):
        a1 = (a+b)/2
        b1 = math.sqrt(a*b)
        E -= i * math.pow((a-b)/2, 2)
        i *= 2
        a = a1
        b = b1

    Fk = math.pi / (2*a)
    return Fk, E * Fk


def getElipticIntegralFirstAndSecondV2(c):
    k = c**2
    Fk = ellipk(k)
    Ek = ellipe(k)
    return Fk, Ek


def getResistance(I, dw):
    k = dw * 1.09

    totalInduc = 0
    nLayer = 1
    totalWireLen = 0
    N = 0
    r0 = (INNER_DIAMETER + k) / 2
    layerTurns = math.floor(COIL_LENGTH/k)
    g = math.exp(-0.25) * dw / 2
    while (totalInduc < I):
        N += 1
        Nc = (N-1) % layerTurns
        nLayer = math.floor((N-1)/layerTurns)

        nx = Nc * k
        ny = r0 + k * nLayer
        Lns = getMutalInductance(ny, ny, g, 0)
        totalWireLen = totalWireLen + 2 * math.pi * ny
        M = 0
        if (N > 1):
            j = N
            while (j >= 2):
                Jc = (j-2) % layerTurns
                jx = Jc * k
                jLayer = math.floor((j-2)/layerTurns)
                jy = r0 + k*jLayer
                M += 2 * getMutalInductance(ny, jy, nx-jx, g)
                j -= 1

        totalInduc += Lns + M

    return (MATERIAL_RESISTIVITY * totalWireLen * 4) / (math.pi * math.pow(dw, 2))


def getAny(I, dw):
    k = dw * 1.09

    totalInduc = 0
    nLayer = 1
    totalWireLen = 0
    N = 0
    r0 = (INNER_DIAMETER + k) / 2
    layerTurns = math.floor(COIL_LENGTH/k)
    g = math.exp(-0.25) * dw / 2

    mass = 0
    while (totalInduc < I):
        # [Max acceptable turns]
        if (N > TURNS_MAX):
            return 0, -1, 0

        N += 1

        nLayer, modN = divmod((N-1), layerTurns)
        nx = modN * k
        ny = r0 + k * nLayer
        Lns = getMutalInductance(ny, ny, g, 0)
        totalWireLen = totalWireLen + 2 * math.pi * ny

        # [Performance skip when over targeted mass]
        if (0.25 * 8.96 * math.pi * dw * dw * totalWireLen > MASS_MAX):
            return 0, -1, 0

        M = 0
        # [Calculate inductance between all turns]
        if (N > 1):
            j = N
            while (j >= 2):
                jLayer, modJ = divmod(j-2, layerTurns)
                jx = modJ * k
                jy = r0 + k * jLayer
                M += 2 * getMutalInductance(ny, jy, nx-jx, g)
                j -= 1

        totalInduc += Lns + M

        mass = 0.25 * 8.96 * math.pi * dw * dw * totalWireLen
        dcTotalResistance = (MATERIAL_RESISTIVITY *
                             totalWireLen * 4) / (math.pi * math.pow(dw, 2))

    return dcTotalResistance, mass, N


def getQfactor(L, R):
    baseL = L*10e-7
    return (2 * math.pi * FREQ * baseL) / R
